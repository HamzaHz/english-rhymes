﻿using UnityEngine;
using System.Collections;

public class BalloonTapped : MonoBehaviour {

	public GameObject burstImageObject;
	public GameObject burstSoundSource;

	public AudioClip tappedAudio;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator DeleteGameObject(GameObject gameObject, GameObject burstImageObject, float time)
	{
		yield return new WaitForSeconds(time);
		Destroy (burstImageObject);
		Destroy (gameObject);
	}
	
	void PlayBurstSound () {
//		GameObject burst = (GameObject) Instantiate (burstSoundSource, Vector3.zero, Quaternion.identity);
//		burst.GetComponent<AudioSource> ().enabled = true;
//		burst.GetComponent<AudioSource> ().Play ();
//		StartCoroutine ((DeleteGameObject(null, burst, 0.5f)));
		AudioSource.PlayClipAtPoint (tappedAudio, Vector3.zero, 0.35f);
	}

	void OnMouseDown() {
		if (this.gameObject.name == "Balloon4(Clone)" || this.gameObject.name == "Balloon1(Clone)" || this.gameObject.name == "Balloon2(Clone)" || this.gameObject.name == "Balloon3(Clone)" || this.gameObject.name == "Balloon10(Clone)") {
			Instantiate(burstImageObject, this.gameObject.transform.position + new Vector3(0, +0.5f, 0), Quaternion.identity);
			//GameObject burst = Instantiate(burstImageObject, this.gameObject.transform.position + new Vector3(0, +0.5f, 0), Quaternion.identity) as GameObject;
			//this.gameObject.GetComponent<SpriteRenderer>().sprite = null;
			//this.gameObject.GetComponent<Animator>().StopPlayback();
			PlayBurstSound ();
			//StartCoroutine ((DeleteGameObject(this.gameObject, burst, 0.1f)));
			Destroy (this.gameObject);
		}
	}
}