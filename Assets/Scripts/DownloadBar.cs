﻿using UnityEngine;
using System.Collections;

public class DownloadBar : MonoBehaviour {

    GameObject DownloadProgress;
    GameObject PercentBG;
    float[] DownloadProgressPlusPosition = new float[2]
    {
        -1.227f,0.048f
    };
    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(this);
        DownloadProgress = GameObject.Find("DownloadProgress");
        //DownloadProgressCurrentScale/
    }
	
	// Update is called once per frame
	void Update () {
	    if(DownloadProgress)
        {
            //Debug.Log("asdas:"+ this.transform.localPosition.x+","+ this.transform.localPosition.y);
            DownloadProgress.transform.localPosition = new Vector3(this.transform.localPosition.x + DownloadProgressPlusPosition[0], this.transform.localPosition.y + DownloadProgressPlusPosition[1], this.transform.localPosition.z);
        }
        
    }
}