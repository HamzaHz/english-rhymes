﻿using UnityEngine;
using System.Collections;

public class IAPDialogScript : MonoBehaviour {

	public Sprite lockSprite;
	public Sprite unLockSprite;

	public GameObject loadingObj;

	// Use this for initialization
	void Start () {
		if (this.gameObject.name == "ad free version") {
			if(IAPManager.isPurchasedAdFreeVersion == 1) {
				//this.gameObject.AddComponent<SpriteRenderer>();
				this.gameObject.GetComponent<SpriteRenderer>().sprite = lockSprite;
				//this.gameObject.GetComponent<SpriteRenderer>().sortingLayerName = "IAPScene";
				//this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 1;
			} else {
				this.gameObject.GetComponent<SpriteRenderer>().sprite = unLockSprite;
			}
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CloseDialog() {
		GameStateManager.GameState = GameState.Playing;
		GameObject dialog = GameObject.FindGameObjectWithTag("IAPDialog");
		Destroy(dialog);
	}


	void OnMouseDown() {
				if (GameStateManager.GameState == GameState.Popup) {
						if (this.gameObject.name == "back") {
								CloseDialog ();
								GameStateManager.GameState = GameState.Playing;
								return;
						}
		
						if (this.gameObject.name == "ad free version") {
								//Do something for first id
								if (IAPManager.isPurchasedAdFreeVersion == 1) {
										return;
								}
								Instantiate (loadingObj, Vector3.zero, Quaternion.identity);
								GameStateManager.GameState = GameState.Loading;
								//GameObject goIAP = GameObject.FindGameObjectWithTag ("IAPDialog");
								//goIAP.GetComponent<UnibillDemo> ().PurchaseClicked (0);
								
								return;
						}
		
						if (this.gameObject.name == "more rhymes") {
								//Do something for second id
								Instantiate (loadingObj, Vector3.zero, Quaternion.identity);
								GameStateManager.GameState = GameState.Loading;
								//GameObject goIAP = GameObject.FindGameObjectWithTag ("IAPDialog");
								//goIAP.GetComponent<UnibillDemo> ().PurchaseClicked (1);
								
								return;
						}
		
						if (this.gameObject.name == "restore") {
								//Do somthing for restore
								Instantiate (loadingObj, Vector3.zero, Quaternion.identity);
								GameStateManager.GameState = GameState.Loading;
								//GameObject goIAP = GameObject.FindGameObjectWithTag ("IAPDialog");
								//goIAP.GetComponent<UnibillDemo> ().RestoredClicked ();
								return;
						}
		
				}
		}
}
