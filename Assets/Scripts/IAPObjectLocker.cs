﻿using UnityEngine;
using System.Collections;

public class IAPObjectLocker : MonoBehaviour {
	
	public Sprite[] lockSprites;

	// Use this for initialization
	void Start () {
		LockSprite ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void LockSprite () {

		if (IAPManager.isPurchasedFullVersion == 0) {
			if(this.gameObject.tag == "2") {
				this.gameObject.GetComponent<SpriteRenderer>().sprite = lockSprites[0];
			} else if(this.gameObject.tag == "8") {
				this.gameObject.GetComponent<SpriteRenderer>().sprite = lockSprites[1];
			} else if(this.gameObject.tag == "9") {
				this.gameObject.GetComponent<SpriteRenderer>().sprite = lockSprites[2];
			} else if(this.gameObject.tag == "11") {
				this.gameObject.GetComponent<SpriteRenderer>().sprite = lockSprites[3];
			} else if(this.gameObject.tag == "13") {
				this.gameObject.GetComponent<SpriteRenderer>().sprite = lockSprites[4];
			} else if(this.gameObject.tag == "14") {
				this.gameObject.GetComponent<SpriteRenderer>().sprite = lockSprites[5];
			} else if(this.gameObject.tag == "16") {
				this.gameObject.GetComponent<SpriteRenderer>().sprite = lockSprites[6];
			} else if(this.gameObject.tag == "17") {
				this.gameObject.GetComponent<SpriteRenderer>().sprite = lockSprites[7];
			} else if(this.gameObject.tag == "18") {
				this.gameObject.GetComponent<SpriteRenderer>().sprite = lockSprites[8];
			} else if(this.gameObject.tag == "20") {
				this.gameObject.GetComponent<SpriteRenderer>().sprite = lockSprites[9];
			} else if(this.gameObject.tag == "24") {
				this.gameObject.GetComponent<SpriteRenderer>().sprite = lockSprites[10];
			}
		}
	}
}
