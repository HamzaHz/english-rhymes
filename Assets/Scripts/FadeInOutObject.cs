﻿using UnityEngine;
using System.Collections;

public class FadeInOutObject : MonoBehaviour {
	// Use this for initialization
	void Start () {
		StartCoroutine("FadeOut");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator FadeOut()
	{
		var rend = this.gameObject.GetComponent<SpriteRenderer> ();
		//Debug.Log ("Fading Out");
		for (float i = 0.7176470588235294f; i >= 0; i -= 0.01f) {
			rend.color = new Color (rend.color.r, rend.color.g, rend.color.b, i);
			yield return new WaitForSeconds (0.01f);
		}
		
		rend.color = new Color (rend.color.r, rend.color.g, rend.color.b, 0);
		
		StartCoroutine ("FadeIn");
	}
	
	IEnumerator FadeIn()
	{
		var rend = this.gameObject.GetComponent<SpriteRenderer>();
		//Debug.Log("Fading In");
		for (float i = 0.1f; i <= 1; i += 0.01f)
		{
			rend.color = new Color(rend.color.r, rend.color.g, rend.color.b, i);
			yield return new WaitForSeconds(0.01f);
		}
		
		rend.color = new Color(rend.color.r, rend.color.g, rend.color.b, 1);
		
		StartCoroutine("FadeOut");
	}
}