﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.SceneManagement;

public class LoadingManager : MonoBehaviour {

    float progress = 0f;
    int Loading = 0;
    int[] LoadFinished = new int[20];
    int[] LoadFinished2 = new int[20];
    int[] LoadRequest = new int[20];

    string LoadURL = "Http://kidsmasterapp.com/rhymes/";
    //string LoadURL = "Http://localhost/Hamza/";
    string[] FileName = new string[20]
    {
        "baba_black_sheep","five_little_monkey","grand_old_duke","hey_diddle","humpty_dumpty",
        "i_had_a_little_nut_tree","if_u_r_happy","itsy_bitsy_spider","jack_and_jill","jack_horner",
        "jingle_bell","little_boy_blue","london_bridge","mary_had_a_little_lamb","mary_quite_contrary",
        "old_mcdonald","one_two_buckle_my_shoe","pussy_cat_pussy_cat","ring_a_roses","twinkle_star"
    };
    string LocalURL ="";


    public AudioClip[] Sound = new AudioClip[20];

    int Done = 0;

    public Transform DownloadProgress;
    float DownloadProgressDefaultXScale=0.5f;

    float PercentBGRangeSize = 2.028f;

    float[] PercentBGPlusPosition = new float[2]
    {
        -1.099f,0.255f
    };

    public Transform PercentBG;
    public Transform DownloadBar;

    public Transform PercentText;
    TextMesh thePercentText;

    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(this);
		SceneManager.LoadScene("StartScreen1");

        thePercentText = PercentText.GetComponent<TextMesh>();
        
        PercentText.GetComponent<Renderer>().sortingLayerName = "IAPScene";
        PercentText.GetComponent<Renderer>().sortingOrder = 2;
    }


	// Update is called once per frame
	void Update () {
        //Debug.Log("Done:" + Done);

        if (PercentBG)
        {
            //Debug.Log("asdas:"+ this.transform.localPosition.x+","+ this.transform.localPosition.y);
            PercentBG.transform.localPosition = new Vector3(DownloadBar.localPosition.x + PercentBGPlusPosition[0]+(progress* PercentBGRangeSize), DownloadBar.localPosition.y + PercentBGPlusPosition[1], DownloadBar.localPosition.z);
            if(PercentText)
            {
                PercentText.transform.localPosition = new Vector3(DownloadBar.localPosition.x + PercentBGPlusPosition[0] + (progress * PercentBGRangeSize), DownloadBar.localPosition.y + PercentBGPlusPosition[1], DownloadBar.localPosition.z);
                thePercentText.text = (progress*100f).ToString("N0")+"%";
            }
        }

        if (Done==0)
        {
			int Completed = 1;
            for (int i = 0; i <= FileName.Length - 1; i++)
            {
                if (LoadFinished[i] == 0)
                {
                    Completed = 0;
                    break;
                }
            }
            for (int i=0; i <= FileName.Length-1; i ++)
			{
				if(Loading==0)
				{
                    if (LoadRequest[i] == 1)
                    {
                        if (LoadFinished[i] == 0)
                        {
                            LocalURL = pathForDocumentsFile(FileName[i]) + ".mp3";
                            if (System.IO.File.Exists(LocalURL))
                            {
                                Loading = 1;
                                LoadFinished[i] = 1;
                                LoadRequest[i] = 0;
                                StartCoroutine(LoadLocalResources(i, LocalURL));
                            }
                            else
                            {
                                Loading = 1;
                                LoadFinished[i] = 1;
                                LoadRequest[i] = 0;
                                StartCoroutine(LoadResources(i, LocalURL));
                            }
                        }
                    }
				}
			}
			if(Completed==1)
			{
				Done = 1;
				//SceneManager.LoadScene("StartScreen1");
			}
		}
	}
    public int CheckSound(string sFileName)
    {
        int Available = 0;
        int No = 0;
        for (int i = 0; i <= FileName.Length - 1; i++)
        {
            if (sFileName == FileName[i])
            {
                No = i;
                if (LoadFinished[No] == 0)
                {
                    if(LoadRequest[No]==0)
                    {
                        LoadRequest[No] = 1;
                        
                    }
                }
                else if (LoadFinished[No] == 1)
                {
                    if (LoadFinished2[No] == 1)
                    {
                        Available = 1;
                    }
                }
                break;
            }
        }
        //Debug.Log("NoSound:" + No);
        return Available;
    }
    public int GetAudioClipNumber(string sFileName)
    {
        int No = 0;
        for (int i = 0; i <= FileName.Length - 1; i++)
        {
            if (sFileName == FileName[i])
            {
                No = i;
                break;
            }        
        }
        //Debug.Log("NoSound:" + No);
        return No;
    }
    public IEnumerator LoadLocalResources(int No, string sLocalURL)
    {
        string ResourceLink = "file://" + sLocalURL;
        WWW www = new WWW(ResourceLink);
        yield return www;

        if (www.error != null)
        {
            LoadFinished2[No] = 0;
            LoadFinished[No] = 0;
            LoadRequest[No] = 1;
        }
        else
        {
            Sound[No] = www.GetAudioClip(false);
            LoadFinished2[No] = 1;
        }

        
        Loading = 0;
    }
    public IEnumerator LoadResources(int No, string sLocalURL)
    {
        GameObject DownloadBar;
        DownloadBar = GameObject.Find("DownloadBar");
        if (DownloadBar)
        {
            DownloadBar.SendMessage("SetPosition", "50||50");
        }
        string ResourceLink = LoadURL+FileName[No]+".mp3";
        //Debug.Log("ResourceLink:" + ResourceLink);
        WWW www = new WWW(ResourceLink);
        //yield return www;
        progress = 0f;
        DownloadProgress.localScale = new Vector3(progress* DownloadProgressDefaultXScale, DownloadProgress.localScale.y, DownloadProgress.localScale.z);
        while (!www.isDone)
        {
            progress = www.progress;
            DownloadProgress.localScale = new Vector3(progress * DownloadProgressDefaultXScale, DownloadProgress.localScale.y, DownloadProgress.localScale.z);
            //Debug.Log("progress:" + progress+","+ No + "," + sLocalURL);
            yield return null;
        }
        yield return www;
        progress = 1.0f;
        DownloadProgress.localScale = new Vector3(progress * DownloadProgressDefaultXScale, DownloadProgress.localScale.y, DownloadProgress.localScale.z);

        if (DownloadBar)
        {
            DownloadBar.SendMessage("SetPosition", "-50||-50");
        }

        
        //Debug.Log("www.error:" + www.error);
        if (www.error!=null)
        {
            LoadFinished2[No] = 0;
            LoadFinished[No] = 0;
            LoadRequest[No] = 1;
        }
        else
        {
            Sound[No] = www.GetAudioClip(false);
            LoadFinished2[No] = 1;

            #if !UNITY_WEBPLAYER
                File.WriteAllBytes(sLocalURL, www.bytes);
            #endif
        }


        Loading = 0;
    }
    public string pathForDocumentsFile(string filename)
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);
            path = path.Substring(0, path.LastIndexOf('/'));
            return Path.Combine(Path.Combine(path, "Documents"), filename);
        }

        else if (Application.platform == RuntimePlatform.Android)
        {
            string path = Application.persistentDataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            return Path.Combine(path, filename);
        }

        else
        {
            string path = Application.dataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            return Path.Combine(path, filename);
        }
    }
}
