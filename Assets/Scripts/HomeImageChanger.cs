﻿using UnityEngine;
using System.Collections;

public class HomeImageChanger : MonoBehaviour {

	public Sprite hSmallDark;
	public Sprite hLargeDark;
	public Sprite hLargeLight;
	public Sprite hSmallLight;

	public AudioClip tick;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown() {
		SpriteRenderer rend = this.gameObject.GetComponent<SpriteRenderer>();
		if (this.gameObject.name == "houseSmall") {
			if(rend.sprite.name == hSmallDark.name) {
				rend.sprite = hSmallLight;
			} else {
				rend.sprite = hSmallDark;
			}

		} else if(this.gameObject.name == "houseBig") {

			if(rend.sprite.name == hLargeDark.name) {
				rend.sprite = hLargeLight;
			} else {
				rend.sprite = hLargeDark;
			}
		}

		AudioSource.PlayClipAtPoint(tick, Vector3.zero);
	}
}
