﻿using UnityEngine;
using System.Collections;

public class DontDestroyRevmob : MonoBehaviour {

	public static GameObject goRevmob;
	
	void Awake() {
		if (goRevmob) {
			Destroy(this.gameObject);
		}
	}
	
	// Use this for initialization
	void Start () {
		goRevmob = this.gameObject;
		DontDestroyOnLoad (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
