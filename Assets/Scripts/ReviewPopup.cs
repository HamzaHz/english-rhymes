﻿using UnityEngine;
using System.Collections;

public class ReviewPopup : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown() {
		if (this.gameObject.name == "Accept") {
			AcceptReviewPopupTapped();
		} else if(this.gameObject.name == "Reject") {
			RejectReviewPopupTapped();
		}
	}

	public void RejectReviewPopupTapped() {
		GameObject popupScreen = GameObject.FindGameObjectWithTag ("ReviewPopup");
		Destroy(popupScreen);
		//GameStateManager.GameState = GameState.Dead;
	}
	
	public void AcceptReviewPopupTapped() {
		CallReviewsURL ();
		RejectReviewPopupTapped ();
		PlayerPrefs.SetInt ("isRhymesPlay5Times", -30);
	}
	
	void CallReviewsURL() {
		
		if (Application.platform == RuntimePlatform.Android)
			Application.OpenURL("market://details?id=com.e90studios.englishrhymes");
		else if (Application.platform == RuntimePlatform.IPhonePlayer)
			Application.OpenURL("itms-apps://itunes.apple.com/us/app/english-rhymes/id1054786724?ls=1&mt=8");
			//Application.OpenURL("https://itunes.apple.com/us/app/balloon-smasher/id991614658?ls=1&mt=8");
		else
			Application.OpenURL("https://itunes.apple.com/us/app/nursery-rhymes-kids-songs/id975040563?mt=8");
	}
}
