﻿using UnityEngine;
using System.Collections;

public class GamePlay : MonoBehaviour {

	void Awake() {
		
		if (DontDestroyBGMusic.goBGMusic != null) {
			//Destroy(DontDestroyBGMusic.goBGMusic.gameObject);
			DontDestroyBGMusic.goBGMusic.gameObject.GetComponent<AudioSource>().Pause();
			//DontDestroyBGMusic.goBGMusic = null;
		}
	}

	// Use this for initialization
	void Start () {
		int isRhymesPlay5Times = PlayerPrefs.GetInt ("isRhymesPlay5Times");
		if (isRhymesPlay5Times > 10) {
			//Application.LoadLevelAdditive(5);

			// "isRhymesPlay5times" is also setting in ReviewPopup script
			PlayerPrefs.SetInt ("isRhymesPlay5Times", 0);
		} else {
			PlayerPrefs.SetInt ("isRhymesPlay5Times", isRhymesPlay5Times + 1);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}


}
