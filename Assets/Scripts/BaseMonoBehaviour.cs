﻿using UnityEngine;
using System.Collections;

public class BaseMonoBehaviour : MonoBehaviour {

	public enum GameStates
	{
		Playing,
		Paused,
		Over
	}

	public enum GameType
	{
		Practice,
		Quiz,
		Challenge
	}

	// Use this for initialization
	protected virtual void Start () {
				if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
						GameObject adManager = GameObject.Find ("GoogleAdManager");
						adManager.GetComponent<GoogleAdManager> ().ShowBanner ();
				}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
