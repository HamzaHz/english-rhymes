﻿using UnityEngine;
using System.Collections;

public class TestingForSize : MonoBehaviour {

	public float xPercentage;

	public float yPercentage;

	// Use this for initialization
	void Start () {

		//Setting size of image
		/*var size = transform.renderer.bounds.size;

		size.x = width * aspectRatio;

		size.y = height * aspectRatio;

		var newSize = new Vector3 (size.x, size.y, size.z);

		Vector3 temp = Camera.main.ScreenToWorldPoint (newSize);

		transform.renderer.bounds.size = temp;*/
	}
	
	// Update is called once per frame
	void Update () {

		var width = Screen.width;
		var height = Screen.height;
		
		//var aspectRatio = width / height;
		
		var  pos = new Vector3((xPercentage/100)*width, (yPercentage/100)*height, 1);
		
		transform.position = Camera.main.ScreenToWorldPoint (pos);
	
	}
    void SetPosition(string tempParam)
    {
        //Debug.Log("SetPosition:" + tempParam);
        string[] tempIsiParam = tempParam.Split(new string[] { "||" }, System.StringSplitOptions.None);
        xPercentage = int.Parse(tempIsiParam[0]);
        yPercentage = int.Parse(tempIsiParam[1]);
    }
}
