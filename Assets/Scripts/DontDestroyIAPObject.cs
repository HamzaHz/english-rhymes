﻿using UnityEngine;
using System.Collections;

public class DontDestroyIAPObject : MonoBehaviour {

	public static GameObject goIAP;
	
	void Awake() {
		if (goIAP) {
			Destroy(this.gameObject);
		}
	}
	
	// Use this for initialization
	void Start () {
		goIAP = this.gameObject;
		DontDestroyOnLoad (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
