﻿using UnityEngine;
using System.Collections;

public class SpawnerScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        //SpawnObject = SpawnObjects[Random.Range(0, SpawnObjects.Length)];
		//timeMin = 1.2f;
		//timeMax = 1.5f;

        Spawn();

		isGameStarted = true;

		GameStateManager.GameState = GameState.Playing;
    }

    void Spawn()
    {
		SpawnObject = SpawnObjects[Random.Range(0, SpawnObjects.Length)];

        if (GameStateManager.GameState == GameState.Playing)
        {
            //random y position
            float y = Random.Range(-1.0f, 1.0f);
            GameObject go = Instantiate(SpawnObject, this.transform.position + new Vector3(0, y, 0), Quaternion.identity) as GameObject;

			//Invoke ("ChangeSpawnTime", 40);
        }
        Invoke("Spawn", Random.Range(timeMin, timeMax));
    }


	void ChangeSpawnTime()
	{
		if (isGameStarted) {
						//timeMin = 0.6f;

						//timeMax = 0.7f;

						//GetReadyObject.SetActive (true);

						//GetReadyObject.transform.position = this.gameObject.transform.position;

						Debug.Log ("ChangeSpawnTime");
						
						isGameStarted= false;
				}
		
	}

	void DeactivateReadyImage () 
	{
		//GetReadyObject.SetActive (false);
	}

    private GameObject SpawnObject;
    public GameObject[] SpawnObjects;

	//public GameObject GetReadyObject;

	public bool isGameStarted;

    public float timeMin;
	public float timeMax;
}
