﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.iOS;

public class AppNotificationsBehavior : MonoBehaviour
{
    public static AppNotificationsBehavior Instance;

		string[] arrayMessages = new string[5];



    void Awake()
    {
		        if (Instance != null)
		            DestroyImmediate(gameObject);
		        else
		        {
		            DontDestroyOnLoad(gameObject);
		            Instance = this;
		        }
    }

    // Use this for initialization
    void Start()
    {
				arrayMessages[0] = "Sing Baba Black Sheep with English Rhymes";
				arrayMessages[1] = "Sing Twinkle Twinkle Little Star with English Rhymes";
				arrayMessages[2] = "Lets sing and play with English Rhymes";
				arrayMessages[3] = "Lets sing Old McDonald with English Rhymes";
				arrayMessages[4] = "We miss you at English Rhymes. Come and sing With English Rhymes";
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnApplicationPause()
    {
        ClearNotification();
        ScheduleNotifications();
    }

    void ScheduleNotifications()
    {
        for (int i = 1; i <= 360; i++)
        {
			System.Random random = new System.Random ();
			int index = random.Next (0, 5);
			ScheduleNotificationsWithText(arrayMessages[index], System.DateTime.Now.AddDays(i));
        }
        
    }

    void ScheduleNotificationsWithText(string text, System.DateTime fireDate)
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
#if UNITY_IOS
            Debug.Log("Scheduled with text: " + text);
            UnityEngine.iOS.LocalNotification notification = new UnityEngine.iOS.LocalNotification
            {
                fireDate = fireDate,
                alertAction = "Alert",
                alertBody = text,
                hasAction = false,
                soundName = "notify.mp3"
					
            };
            //UnityEngine.LocalNotification
            UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notification);
            UnityEngine.iOS.NotificationServices.RegisterForNotifications(NotificationType.Alert, false);
            UnityEngine.iOS.NotificationServices.RegisterForNotifications(NotificationType.Badge, false);
			UnityEngine.iOS.NotificationServices.RegisterForNotifications(NotificationType.Sound, true);
#endif
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            //AndroidLocalNotification.SendNotification(GetAndroidNotificationId(), (long)fireDate.Subtract(DateTime.Now).TotalSeconds, "Classic Duck Hunting", text, Color.clear);

            var notificationId = AndroidNotificationManager.instance.ScheduleLocalNotification("English Rhymes", text,
                (int)fireDate.Subtract(DateTime.Now).TotalSeconds);
            
            //EtceteraAndroid.scheduleNotification((long)fireDate.Subtract(DateTime.Now).TotalSeconds, "Classic Duck Hunting", text,
                //text, string.Empty);

            var ids = PlayerPrefs.GetString("ANIds", string.Empty);
            ids += "," + notificationId.ToString();
            PlayerPrefs.SetString("ANIds", ids);
            PlayerPrefs.Save();
        }
    }

    //private int GetAndroidNotificationId(bool increment = true)
    //{
    //    var lastNotificationId = PlayerPrefs.GetInt("lastNotificationId", 0);
    //    if (increment)
    //    {
    //        lastNotificationId++;
    //        PlayerPrefs.SetInt("lastNotificationId", lastNotificationId);
    //        PlayerPrefs.Save();
    //    }
    //    return lastNotificationId;
    //}

    void ClearNotification()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
#if UNITY_IOS
            if (UnityEngine.iOS.NotificationServices.scheduledLocalNotifications.Length > 0)
            {
                UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications();
                UnityEngine.iOS.NotificationServices.ClearLocalNotifications();
            }
#endif
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            //var lastNotificationId = GetAndroidNotificationId(false);

            //for (int i = lastNotificationId; i > Math.Max(0, lastNotificationId - 10); i--)
            //{
            //    AndroidLocalNotification.CancelNotification(i);    
            //}

            //var idStr = PlayerPrefs.GetString("ANIds", string.Empty);
            //if (!string.IsNullOrEmpty(idStr))
            //{
            //    var ids = idStr.Split(',').ToList();
            //    foreach (var id in ids)
            //    {
            //        if (!string.IsNullOrEmpty(id))
            //            EtceteraAndroid.cancelNotification(Convert.ToInt32(id));                
            //    }
            //    PlayerPrefs.DeleteKey("ANIds");
            //    PlayerPrefs.Save();
            //}

            AndroidNotificationManager.instance.CancelAllLocalNotifications();

            
            
        }
    }
}
