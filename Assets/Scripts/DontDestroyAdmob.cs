﻿using UnityEngine;
using System.Collections;

public class DontDestroyAdmob : MonoBehaviour {

	public static GameObject goAdmob;

	void Awake() {
		if (goAdmob) {
			Destroy(this.gameObject);
		}
	}
	
	// Use this for initialization
	void Start () {
		goAdmob = this.gameObject;
		DontDestroyOnLoad (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
