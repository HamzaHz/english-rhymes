﻿using UnityEngine;
using System.Collections;

public class ParallaxScroll : MonoBehaviour {

	public Renderer background;

	public Renderer cloud;

	//public Renderer sky;
	
	public float backgroundSpeed = 0.02f;

	public float cloudSpeed = 0.02f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		float backgroundOffset = Time.timeSinceLevelLoad * backgroundSpeed;
		float cloudOffset = Time.timeSinceLevelLoad * cloudSpeed;

		//sky.transform.RotateAround(background.transform.position, Vector3.forward, 50 * Time. fixedDeltaTime);
		
		background.material.mainTextureOffset = new Vector2(backgroundOffset, 0);
		cloud.material.mainTextureOffset = new Vector2(cloudOffset, 0);
	
	}
}
