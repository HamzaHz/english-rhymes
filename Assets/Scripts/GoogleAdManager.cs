﻿using UnityEngine;
using System.Collections;

public class GoogleAdManager : MonoBehaviour {


		public static GoogleAdManager Instance;

		public GoogleMobileAdBanner banner_smart;

		//--------------------------------------
		// INITIALIZE
		//--------------------------------------

		void Awake()
		{
				if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
						if (Instance != null)
								DestroyImmediate (gameObject);
						else {
								DontDestroyOnLoad (gameObject);
								Instance = this;
						}
				}
		}

		void Start() {

				if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {

						//Required
						GoogleMobileAd.Init ();


						//Optional, add data for better ad targeting
						GoogleMobileAd.SetGender (GoogleGender.Unknown);
						GoogleMobileAd.AddKeyword ("game");
						GoogleMobileAd.AddKeyword ("balloon");
						GoogleMobileAd.AddKeyword ("kids");
						GoogleMobileAd.AddKeyword ("learning");
						GoogleMobileAd.SetBirthday (2000, AndroidMonth.MARCH, 18);
						GoogleMobileAd.TagForChildDirectedTreatment (false);

						//Causes a device to receive test ads. The deviceId can be obtained by viewing the device log output after creating a new ad
						//Fill your test device in the plugin setting, or you can add your device using example code bellow

						GoogleMobileAd.AddTestDevice ("733770c317dcbf4675fe870d3df9ca42");

						//listening for InApp Event
						//You will only receive in-app purchase (IAP) ads if you specifically configure an IAP ad campaign in the AdMob front end.
						GoogleMobileAd.OnAdInAppRequest += OnInAppRequest;


						CallBanner ();
						//InvokeRepeating ("CallInterstitial", 10, 90);
				}
		}

		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		public void CallInterstitial () {
				GoogleMobileAd.StartInterstitialAd ();
				GoogleMobileAd.LoadInterstitialAd ();
				GoogleMobileAd.OnInterstitialLoaded += OnInterstitialLoaded;
		}

		//--------------------------------------
		//  PRIVATE METHODS
		//--------------------------------------

		public void CallBanner () {
				banner_smart = GoogleMobileAd.CreateAdBanner(TextAnchor.LowerLeft, GADBannerSize.SMART_BANNER);

				//listening for banner to load example using C# actions:
				banner_smart.OnLoadedAction += OnBannerLoadedAction;

				banner_smart.ShowOnLoad = true;
				Debug.Log ("Creating banner");
		}

		public void DestroyBanner(GoogleMobileAdBanner banner) {
				Debug.Log ("Destroying banner");
				GoogleMobileAd.DestroyBanner(banner.id);
		}

		public void RefreshBanner (GoogleMobileAdBanner banner) {
				banner.Refresh();
		}

		public void ShowBanner (){
				if (!banner_smart.IsOnScreen) {
						Debug.Log ("Showing banner");
						banner_smart.Show ();
				}
		}

		public void HideBanner (){
				Debug.Log ("Hiding banner");
				banner_smart.Hide ();
		}

		//--------------------------------------
		//  EVENTS
		//--------------------------------------


		private void OnInAppRequest(string productId) {
				Debug.Log ("In App Request for product Id: " + productId + " received");


				//Then you should perfrom purchase  for this product id, using this or another game billing plugin
				//Once the purchase is complete, you should call RecordInAppResolution with one of the constants defined in GADInAppResolution:

				GoogleMobileAd.RecordInAppResolution(GADInAppResolution.RESOLUTION_SUCCESS);

		}


		//--------------------------------------
		//  ACTIONS
		//--------------------------------------

		private void OnInterstitialLoaded () {
				Debug.Log("OnInterstitialLoaded catched with C# Actions usage");
				GoogleMobileAd.ShowInterstitialAd ();
		}

		void OnOpenedAction (GoogleMobileAdBanner banner) {
				banner.OnOpenedAction -= OnOpenedAction;
				Debug.Log("Banner was just clicked");
		}

		private void OnBannerLoadedAction (GoogleMobileAdBanner banner) {
				banner.OnLoadedAction -= OnBannerLoadedAction;
				banner.Show();
		}



		//--------------------------------------
		//  DESTROY
		//--------------------------------------



}