﻿using UnityEngine;
using System.Collections;

public class ButtonRotationLeft : MonoBehaviour {

	private Vector3 start;
	private Vector3 target;
	
	public float moveY;
	
	public float timeToMove;
	
	// Use this for initialization
	void Start () {
		start = transform.position;
		target = new Vector3 (transform.position.x, transform.position.y + 0.0f, transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void FixedUpdate () {
		
		Vector3 pos = transform.position;
		if (pos != target) {
			transform.position = Vector3.MoveTowards (pos, target, timeToMove);
		} else {
			
			target = start;
			if (pos == start) {
				target = new Vector3(transform.position.x - moveY, transform.position.y - moveY, transform.position.z);
			}
		}
		//transform.RotateAround(transform.position, Vector2.up, rotationSpeed * Time. fixedDeltaTime);
	}
}
