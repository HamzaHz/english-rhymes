﻿using UnityEngine;
using System.Collections;

public class BurstDestroy : MonoBehaviour {

	public float timeToDestroy = 1.0f;

	// Use this for initialization
	void Start () {
		Invoke ("DestroyObject", timeToDestroy);
	}

	void DestroyObject() {
		Destroy (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
