﻿using UnityEngine;
using System.Collections;

public class RotateBackground : MonoBehaviour {

	public Renderer sky;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		sky.transform.RotateAround(sky.transform.position, Vector3.forward, 50 * Time. fixedDeltaTime);
	}
}
