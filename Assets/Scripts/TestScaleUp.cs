﻿using UnityEngine;
using System.Collections;

public class TestScaleUp : MonoBehaviour {

	GameObject Player;

	public GameObject _particleSystem;
	
//	private float playerScaleX;
//	private float playerScaleY;
	
	public float scaleSize = 0.2f;

	public float timeTakenDuringLerp = 1f;

	public float timeTakenOnStay = 1f;
	
	/// <summary>
	/// How far the object should move when 'space' is pressed
	/// </summary>
	
	//Whether we are currently interpolating or not
	private bool _isLerping;
	private bool _isLerpingDown;
	
	//The start and finish positions for the interpolation
	private Vector3 _startPosition;
	private Vector3 _endPosition;
	
	//The Time.time value when we started the interpolation
	private float _timeStartedLerping;

	// Use this for initialization
	void Start () {
		Player = this.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate()
	{
		if(_isLerping)
		{
			//We want percentage = 0.0 when Time.time = _timeStartedLerping
			//and percentage = 1.0 when Time.time = _timeStartedLerping + timeTakenDuringLerp
			//In other words, we want to know what percentage of "timeTakenDuringLerp" the value
			//"Time.time - _timeStartedLerping" is.
			float timeSinceStarted = Time.time - _timeStartedLerping;
			float percentageComplete = timeSinceStarted / timeTakenDuringLerp;
			
			//Perform the actual lerping.  Notice that the first two parameters will always be the same
			//throughout a single lerp-processs (ie. they won't change until we hit the space-bar again
			//to start another lerp)
			Player.transform.localScale = Vector3.Lerp (_startPosition, _endPosition, percentageComplete);
			
			//When we've completed the lerp, we set _isLerping to false
			if(percentageComplete >= 1.0f)
			{
				_isLerping = false;
			}
		}
	}

	void StartScaleUp()
	{
		_isLerping = true;
		_timeStartedLerping = Time.time;
		
		//We set the start position to the current position, and the finish to 10 spaces in the 'forward' direction
		_startPosition = Player.transform.localScale;
		Vector3 vect = new  Vector3(scaleSize, scaleSize, scaleSize);
		_endPosition = Player.transform.localScale + vect;
	}

	void StartScaleDown() {
		_isLerping = true;
		_isLerpingDown = false;
		_timeStartedLerping = Time.time;
		
		//We set the start position to the current position, and the finish to 10 spaces in the 'forward' direction
		_startPosition = Player.transform.localScale;
		Vector3 vect = new  Vector3(scaleSize, scaleSize, scaleSize);
		_endPosition = Player.transform.localScale - vect;
	}

	void OnMouseDown() {
		if(!_isLerping && !_isLerpingDown) {

		StartScaleUp ();
		Instantiate(_particleSystem, this.gameObject.transform.position, Quaternion.identity);
		_isLerpingDown = true;
		
		Invoke ("StartScaleDown", timeTakenDuringLerp + timeTakenOnStay);
		
		}
	}
}
