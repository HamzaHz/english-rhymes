﻿using UnityEngine;
using System.Collections;

public class IAPManager : MonoBehaviour {

	public static int isPurchasedFullVersion;
	public static int isPurchasedAdFreeVersion;

	// Use this for initialization
	void Start () {
		//PlayerPrefs.DeleteAll ();
		string status = GetIAPStatus ();
		if (status == "fullversion") {
			DestroyAdMob();
			isPurchasedFullVersion = 1;
		} else if(status == "adfreeversion") {
			DestroyAdMob();
			isPurchasedAdFreeVersion = 1;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void isPurchaseAdFreeVersion() {
		//Do somthing for ad free version only
		Debug.Log("Purchased Ad Free Version");
		SetIAPStatus (0);
		DestroyAdMob ();
		isPurchasedAdFreeVersion = 1;
		ReloadScene ();
	}
	
	public void isPurchasedFullversion() {
		//Do somthing for full version
		Debug.Log("Purchased Full Version");
		SetIAPStatus (1);
		DestroyAdMob ();
		isPurchasedFullVersion = 1;
		ReloadScene ();
	}
	
	public void isFailedPurchased() {
		//Do something for failed transection
		Debug.Log("Failed to purchased");
		GameObject loading = GameObject.Find("Loading");
		if (loading != null) {
			Destroy(loading);
		}
	}

	void SetIAPStatus(int id) {
		if (id == 1) {
			PlayerPrefs.SetInt ("PurchaseFullVersion", 1);
			return;
		}

		if (id == 0) {
			PlayerPrefs.SetInt("PurchaseAdFreeVersion", 1);
			return;
		}
	}

	public string GetIAPStatus() {
		int idFullVersion = PlayerPrefs.GetInt ("PurchaseFullVersion");
		int idAdFreeVersion = PlayerPrefs.GetInt ("PurchaseAdFreeVersion");

		if (idFullVersion == 1) {
			return "fullversion";
		}

		if (idAdFreeVersion == 1) {
			return "adfreeversion";
		}
		return null;
	}

	void DestroyAdMob() {
		GameObject goAdmob = GameObject.Find("Admob");
		if(goAdmob != null) {
//			GoogleMobileAdsDemoScript demo = goAdmob.GetComponent<GoogleMobileAdsDemoScript> ();
//			if(demo.bannerView!= null) {
//				demo.bannerView.Hide ();
//			}
			Destroy (goAdmob);
		}
	}

	void CloseDialog() {
		GameStateManager.GameState = GameState.Playing;

		GameObject dialog = GameObject.FindGameObjectWithTag("IAPDialog");
		Destroy(dialog);
	}

	void ReloadScene() {
		GameStateManager.GameState = GameState.Playing;
		Application.LoadLevel (Application.loadedLevel);
	}
}
