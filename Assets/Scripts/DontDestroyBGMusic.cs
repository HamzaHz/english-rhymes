﻿using UnityEngine;
using System.Collections;

public class DontDestroyBGMusic : MonoBehaviour {

	public static GameObject goBGMusic;
	
	void Awake() {
		if (goBGMusic != null) {
			Destroy(this.gameObject);
			if(!goBGMusic.GetComponent<AudioSource>().isPlaying)
				goBGMusic.GetComponent<AudioSource>().Play();
		}
	}
	
	// Use this for initialization
	void Start () {
		goBGMusic = this.gameObject;
		DontDestroyOnLoad (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
