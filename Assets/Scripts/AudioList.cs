﻿using UnityEngine;
using System.Collections;

public class AudioList : MonoBehaviour {
	
	public AudioClip[] arrAudio;
	public AudioSource audiosour;


    GameObject LM;
    public LoadingManager LoadingManager;

    // Use this for initialization
    void Start () {
		//audioSource = this.gameObject.GetComponent<AudioSource>;
		Debug.Log ("Audio Player. Audiotapped " + AudioPlayer.audioTapped);

        //		if (AudioPlayer.audioTapped == "baba_black_sheep") {
        //			audiosour.clip = Resources.Load("baba_black_sheep") as AudioClip;
        //						
        //		} else if (AudioPlayer.audioTapped == "five_little_monkey") {
        //			audiosour.clip = Resources.Load("five_little_monkey") as AudioClip;
        //						
        //		} else if (AudioPlayer.audioTapped == "humpty_dumpty") {
        //			audiosour.clip = Resources.Load("humpty_dumpty") as AudioClip;
        //			
        //		} else if (AudioPlayer.audioTapped == "if_u_r_happy") {
        //			audiosour.clip = Resources.Load("if_u_r_happy") as AudioClip;
        //			
        //		} else if (AudioPlayer.audioTapped == "itsy_bitsy_spider") {
        //			audiosour.clip = Resources.Load("itsy_bitsy_spider") as AudioClip;
        //			
        //		} else if (AudioPlayer.audioTapped == "jingle_bell") {
        //			audiosour.clip = Resources.Load("jingle_bell") as AudioClip;
        //			
        //		} else if (AudioPlayer.audioTapped == "london_bridge") {
        //			audiosour.clip = Resources.Load("london_bridge") as AudioClip;
        //			
        //		} else if (AudioPlayer.audioTapped == "mary_had_a_little_lamp") {
        //			audiosour.clip = Resources.Load("mary_had_a_little_lamp") as AudioClip;
        //			
        //		} else if (AudioPlayer.audioTapped == "old_mcdonald") {
        //			audiosour.clip = Resources.Load("old_mcdonald") as AudioClip;
        //			
        //		} else if (AudioPlayer.audioTapped == "one_two_buckle_my_shoe") {
        //			audiosour.clip = Resources.Load("one_two_buckle_my_shoe") as AudioClip;
        //			
        //		} else if (AudioPlayer.audioTapped == "pussy_cat_pussy_cat") {
        //			audiosour.clip = Resources.Load("pussy_cat_pussy_cat") as AudioClip;
        //			
        //		} else if (AudioPlayer.audioTapped == "ring_a_roses") {
        //			audiosour.clip = Resources.Load("ring_a_roses") as AudioClip;
        //			
        //		} else if (AudioPlayer.audioTapped == "twinkle_star") {
        //			audiosour.clip = Resources.Load("twinkle_star") as AudioClip;
        //			
        //		}

        LM = GameObject.Find("LoadingManager");

        LoadingManager = LM.GetComponent<LoadingManager>();
        int SoundAvailable=LoadingManager.CheckSound(AudioPlayer.audioTapped);
        if (SoundAvailable == 1)
        {
            audiosour.clip = LoadingManager.Sound[LoadingManager.GetAudioClipNumber(AudioPlayer.audioTapped)];
            audiosour.Play();
        }
        else
        {
            InvokeRepeating("CheckSound", 0f,1f);
        }
        //Debug.Log("audiosour.clip:" + LoadingManager.Sound[LoadingManager.GetAudioClipNumber(AudioPlayer.audioTapped)].name);
        //audiosour.clip = Resources.Load(AudioPlayer.audioTapped) as AudioClip;

		
	}
    void CheckSound()
    {
        int SoundAvailable = LoadingManager.CheckSound(AudioPlayer.audioTapped);
        if (SoundAvailable == 1)
        {
            audiosour.clip = LoadingManager.Sound[LoadingManager.GetAudioClipNumber(AudioPlayer.audioTapped)];
            audiosour.Play();
            CancelInvoke("CheckSound");
        }
        //Debug.Log("CheckSound");
    }

    // Update is called once per frame
    void Update () {
	
	}
}