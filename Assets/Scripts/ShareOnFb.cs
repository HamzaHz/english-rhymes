﻿namespace Facebook.Unity.Example
{
	using UnityEngine;
	using System.Collections;
	using System;
	using System.Collections.Generic;
	using System.Linq;

	public class ShareOnFb : MonoBehaviour {

		GameObject shareButton;
		Texture2D tex;

		void Start() {
			//SetLikeButtonStatus (false);
			//FB.Init (this.OnCompleteInit);
		}

		void OnCompleteInit() {
			//Do somthing when Facebook initiliaze
			Debug.Log("FB initializing complete");
			SetLikeButtonStatus (true);
		}

		void SetLikeButtonStatus(bool isEnable) {
			if (!isEnable) {
				GetComponent<BoxCollider2D>().enabled = false;
				GetComponent<SpriteRenderer>().enabled = false;
			} else {
				GetComponent<BoxCollider2D>().enabled = true;
				GetComponent<SpriteRenderer>().enabled = true;
			}
		}

		void OnMouseDown() {
//			FB.ShareLink(
//				new Uri("https://developers.facebook.com/"),
//				"Link Share",
//				"Look I'm sharing a link",
//				new Uri("http://i.imgur.com/j4M7vCO.jpg"),
//				callback: this.HandleResult);
						MNP.ShowPreloader ("Loading", "Please wait");
						StartCoroutine( PostFBScreenshot ());
		}
//
//		void HandleResult(IResult result)
//		{
//			if (result == null)
//			{
//				Debug.LogError ("Null Response\n");
//				ShowMessage("null resonse");
//				return;
//			}
//						
//			// Some platforms return the empty string instead of null.
//			if (!string.IsNullOrEmpty(result.Error))
//			{
//				Debug.LogError( "Error\n  Error Response:\n" + result.Error);
//				ShowMessage(result.Error);
//			}
//			else if (result.Cancelled)
//			{
//				Debug.LogError( "Cancelled\n Cancelled Response:\n" + result.RawResult);
//				ShowMessage(result.Error);
//			}
//			else if (!string.IsNullOrEmpty(result.RawResult))
//			{
//				Debug.Log("Success - Check log for details\n Success Response:\n" + result.RawResult);
//				ShowMessage("SuccessFully share");
//			}
//			else
//			{
//				Debug.LogError( "Empty Response\n");
//				ShowMessage("Empthy response");
//			}
//		}

		void ShowMessage(string message) {
				MobileNativeMessage msg = new MobileNativeMessage("Title", message);
				//Uncomment below line when ok delegate handle
				//msg.OnComplete += OnMessageClose;
		}

				private IEnumerator PostFBScreenshot() {


				yield return new WaitForEndOfFrame();
				// Create a texture the size of the screen, RGB24 format
				int width = Screen.width;
				int height = Screen.height;
				tex = new Texture2D( width, height, TextureFormat.ARGB32, false );
				// Read screen contents into the texture
				tex.ReadPixels( new Rect(0, 0, width, height), 0, 0 );
				tex.Apply();


				UM_ShareUtility.FacebookShare("English Nursery Rhymes for Kids", tex);

				Destroy(tex);
				MNP.HidePreloader ();
		}
	}
}