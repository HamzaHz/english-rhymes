﻿using UnityEngine;
using System.Collections;

public class StartScript : BaseMonoBehaviour {

	protected AsyncOperation async;
	
	// Use this for initialization
	protected override void Start () {
		base.Start ();
	}
	
	// Update is called once per frame
	protected void Update()
	{
		if (async != null && async.progress >= 0.8) {
			MNP.HidePreloader ();
			async.allowSceneActivation = true;
		}
	}
	
	private void OnMouseDown()
	{
			StartCoroutine (StartLoadinAsync("Type"));
	}

	protected IEnumerator StartLoadinAsync(string levelName) {
			MNP.ShowPreloader ("Loading", "Please wait");
			async = new AsyncOperation();
			async = Application.LoadLevelAsync(levelName);
			async.allowSceneActivation = false;
			
			if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
					GameObject adManager = GameObject.Find ("GoogleAdManager");
					adManager.GetComponent<GoogleAdManager> ().HideBanner ();
			}
			
			yield return async;
	}
}
