﻿using UnityEngine;
using System.Collections;

public class HouseSmoke : MonoBehaviour {

	public Sprite s1;
	public Sprite s2;
	public Sprite s3;

	public GameObject goSmoke1;
	public GameObject goSmoke2;
	public GameObject goSmoke3;

	bool smoke1;
	bool smoke2;
	bool smoke3;

	// Use this for initialization
	void Start () {
		smoke1 = true;
		StartCoroutine ("ChangeSmoke");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator ChangeSmoke () {
		if(smoke1) {
			//GameObject obj1 = GameObject.Find("smoke-1");
			goSmoke1.GetComponent<SpriteRenderer>().sprite = s1;
			goSmoke2.GetComponent<SpriteRenderer>().sprite = null;
			goSmoke3.GetComponent<SpriteRenderer>().sprite = null;
			smoke2 = true;
			smoke1 = false;
			smoke3 = false;
		} else if(smoke2) {
			//GameObject obj1 = GameObject.Find("smoke-2");
			goSmoke1.GetComponent<SpriteRenderer>().sprite = null;
			goSmoke2.GetComponent<SpriteRenderer>().sprite = s2;
			goSmoke3.GetComponent<SpriteRenderer>().sprite = null;
			smoke2 = false;
			smoke1 = false;
			smoke3 = true;
		} else if(smoke3) {
			//GameObject obj1 = GameObject.Find("smoke-3");
			goSmoke1.GetComponent<SpriteRenderer>().sprite = null;
			goSmoke2.GetComponent<SpriteRenderer>().sprite = null;
			goSmoke3.GetComponent<SpriteRenderer>().sprite = s3;
			smoke2 = false;
			smoke1 = true;
			smoke3 = false;
		}

		yield return new WaitForSeconds (0.5f);
		StartCoroutine ("ChangeSmoke");
	}
}
