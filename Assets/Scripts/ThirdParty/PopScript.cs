﻿using UnityEngine;
using System.Collections;

public class PopScript : MonoBehaviour {

    public AudioClip popSound;

	public Vector3 scaleSize;
	// Use this for initialization
	void Start () {

        gameObject.transform.Rotate(new Vector3(0, 0, Random.Range(0, 180)));

        LeanTween.scale(gameObject, scaleSize, .2f).setOnComplete(() =>
        {
            Destroy(this.gameObject);
        });

        LeanTween.alpha(gameObject, 0, .2f);

        AudioSource.PlayClipAtPoint(popSound, Vector3.zero);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
