﻿using UnityEngine;
using System.Collections;

public class OnMouseDown : MonoBehaviour {

	Touch touch;
	public GameObject burstImageObject;
	public AudioSource burstSoundSource;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction);
		
				if (hit != null && hit.collider != null) {
						//isHit = false;
						if (Application.platform != RuntimePlatform.Android && Application.platform != RuntimePlatform.IPhonePlayer) {
								if (Input.GetButton ("Fire1")) {
					if (hit.collider.gameObject.name == "Button") {
						gameObject.SendMessage ("ButtonTapped", hit.collider.gameObject);
					} else if (hit.collider.gameObject.name == "Balloon4(Clone)" || hit.collider.gameObject.name == "Balloon1(Clone)" || hit.collider.gameObject.name == "Balloon2(Clone)" || hit.collider.gameObject.name == "Balloon3(Clone)" || hit.collider.gameObject.name == "Balloon10(Clone)") {
						gameObject.SendMessage ("BalloonTapped", hit.collider.gameObject);
					} else if (hit.collider.gameObject.name == "BackButton") {
						gameObject.SendMessage ("BackButtonTapped", hit.collider.gameObject);
					} else if (hit.collider.gameObject.name == "NextButton") {
						gameObject.SendMessage ("NextButtonTapped", hit.collider.gameObject);
					} else if (hit.collider.gameObject.name == "BackButtonMenu") {
						gameObject.SendMessage ("BackButtonMenuClicked", hit.collider.gameObject);
					} 
								}
						} else {
								for (int i = 0; i < Input.touchCount; i++) {
										touch = Input.GetTouch (i);
										if (touch.phase == TouchPhase.Began) {
												//Destroy (GameObject.Find (hit.collider.gameObject.name));
						if (hit.collider.gameObject.name == "Button") {
							gameObject.SendMessage ("ButtonTapped", hit.collider.gameObject);
						} else if (hit.collider.gameObject.name == "Balloon4(Clone)" || hit.collider.gameObject.name == "Balloon1(Clone)" || hit.collider.gameObject.name == "Balloon2(Clone)" || hit.collider.gameObject.name == "Balloon3(Clone)" || hit.collider.gameObject.name == "Balloon10(Clone)") {
							gameObject.SendMessage ("BalloonTapped", hit.collider.gameObject);
						} else if (hit.collider.gameObject.name == "BackButton") {
							gameObject.SendMessage ("BackButtonTapped", hit.collider.gameObject);
						} else if (hit.collider.gameObject.name == "NextButton") {
							gameObject.SendMessage ("NextButtonTapped", hit.collider.gameObject);
						} else if (hit.collider.gameObject.name == "BackButtonMenu") {
							gameObject.SendMessage ("BackButtonMenuClicked", hit.collider.gameObject);
						} 
										}
								}
						}
				}
	}

	void ButtonTapped(GameObject tapObject)
	{
		Debug.Log ("Button tag number " + tapObject.tag);

		if (tapObject.CompareTag ("1")) {
			AudioPlayer.audioTapped = "1";

		} else if (tapObject.CompareTag ("2")) {
			AudioPlayer.audioTapped = "2";
			
		} else if (tapObject.CompareTag ("3")) {
			AudioPlayer.audioTapped = "3";
			
		}else if (tapObject.CompareTag ("4")) {
			AudioPlayer.audioTapped = "4";
			
		}else if (tapObject.CompareTag ("5")) {
			AudioPlayer.audioTapped = "5";
			
		}else if (tapObject.CompareTag ("6")) {
			AudioPlayer.audioTapped = "6";
			
		}else if (tapObject.gameObject.CompareTag ("7")) {
			AudioPlayer.audioTapped = "7";
			
		}else if (tapObject.gameObject.CompareTag ("8")) {
			AudioPlayer.audioTapped = "8";
			
		}else if (tapObject.gameObject.CompareTag ("9")) {
			AudioPlayer.audioTapped = "9";
			
		}else if (tapObject.gameObject.CompareTag ("10")) {
			AudioPlayer.audioTapped = "10";
			
		}else if (tapObject.gameObject.CompareTag ("11")) {
			AudioPlayer.audioTapped = "11";
			
		}else if (tapObject.gameObject.CompareTag ("12")) {
			AudioPlayer.audioTapped = "12";
			
		}else if (tapObject.gameObject.CompareTag ("13")) {
			AudioPlayer.audioTapped = "13";
			
		}else if (tapObject.gameObject.CompareTag ("14")) {
			AudioPlayer.audioTapped = "14";
			
		}else if (tapObject.gameObject.CompareTag ("15")) {
			AudioPlayer.audioTapped = "15";
			
		}else if (tapObject.gameObject.CompareTag ("16")) {
			AudioPlayer.audioTapped = "16";
			
		}
		var randomX = UnityEngine.Random.Range (3, 5);
		AutoFade.LoadLevel(randomX ,0.1f,1,Color.black);
	}

	void BalloonTapped(GameObject tapObject) {
		GameObject burst = Instantiate(burstImageObject, tapObject.transform.position + new Vector3(0, +0.5f, 0), Quaternion.identity) as GameObject;
		Destroy (tapObject);
		PlayBurstSound ();
		StartCoroutine ((DeleteGameObject(tapObject, burst)));
		}
	void BackButtonTapped(GameObject tapObject) {
		AutoFade.LoadLevel(0 ,0.2f,1,Color.black);
	}

	IEnumerator DeleteGameObject(GameObject tapObject, GameObject burstImageObject)
	{
		yield return new WaitForSeconds(0.1f);
		Destroy (burstImageObject);
	}

	void PlayBurstSound () {
		burstSoundSource.Play ();
	}

	void NextButtonTapped() {
		int level = Application.loadedLevel;
		AutoFade.LoadLevel(level + 1 ,0.2f,1,Color.black);
		//Application.LoadLevel (level + 1);
		}

	void BackButtonMenuClicked (GameObject obj) {
		int level = Application.loadedLevel;
		AutoFade.LoadLevel(level - 1 ,0.2f,1,Color.black);
		//Application.LoadLevel (level - 1);
		}
}