﻿using UnityEngine;
using System.Collections;

public class pause : MonoBehaviour {

	public Sprite pauseSprite;
	public Sprite playSprite;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown() {
		GameObject obj = GameObject.Find("Audio List");
		if (obj != null) {
			AudioSource source = obj.GetComponent<AudioSource>();
			if(source.isPlaying) {
				PauseAudio(source);
			} else {
				PlayAudio(source);
			}
		}
	}

	void PauseAudio(AudioSource source) {
		source.Pause ();
		this.gameObject.GetComponent<SpriteRenderer> ().sprite = playSprite;
	}

	void PlayAudio(AudioSource source) {
		source.Play ();
		this.gameObject.GetComponent<SpriteRenderer> ().sprite = pauseSprite;
	}
}
