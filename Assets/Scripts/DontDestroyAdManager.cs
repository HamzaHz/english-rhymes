﻿using UnityEngine;
using System.Collections;

public class DontDestroyAdManager : MonoBehaviour {

	public static GameObject goAdManager;
	
	void Awake() {
		if (goAdManager) {
			Destroy(this.gameObject);
		}
	}
	
	// Use this for initialization
	void Start () {
		goAdManager = this.gameObject;
		DontDestroyOnLoad (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
