﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class BackButtonTapped : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown () {
		if (this.gameObject.name == "BackButtonMenu") {
			//int level =  Application.loadedLevel;
            int level = SceneManager.GetActiveScene().buildIndex;
            if(level>0)
            {
                SceneManager.LoadScene(level - 1);
            }
            
			//AutoFade.LoadLevel(level - 1 ,0.2f,1,Color.black);
		} else if (this.gameObject.name == "BackButton") {
            SceneManager.LoadScene(LevelManager.lastLoadedLevel);
            
			//Application.LoadLevel (LevelManager.lastLoadedLevel);
			//AutoFade.LoadLevel(LevelManager.lastLoadedLevel,0.2f, 0.2f,Color.black);
		}
	}
}
