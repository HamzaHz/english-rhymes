﻿using UnityEngine;
using System.Collections;

public class RequestInterstitial : MonoBehaviour {

//	void Awake() {
//		GameObject iapManager = GameObject.Find("IAPManager");
//		if(iapManager != null) {
//			string status = iapManager.GetComponent<IAPManager> ().GetIAPStatus ();
//			if (status == "fullversion" || status == "adfreeversion") {
//				Destroy(this.gameObject);
//			}
//		}
//		//		if (IAPManager.isPurchasedAdFreeVersion == 1 || IAPManager.isPurchasedFullVersion == 1) {
//		//			Destroy(this.gameObject);
//		//		}
//	}

	// Use this for initialization
	void Start () {
		//InvokeRepeating ("Request", 5.0f, 30.0f);
		Request ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Request() {
		AdMobRequest();
	}

	void AdMobRequest() {
		GameObject adMob = GameObject.FindGameObjectWithTag("Admob");
		if (adMob != null) {
			adMob.GetComponent<GoogleAdManager> ().CallInterstitial ();
		}
	}

//	void RevMobInterstitialRequest() {
//		GameObject revMob = GameObject.Find("Revmob");
//		if (revMob != null) {
//			revMob.GetComponent<RevMobSampleAppCSharp> ().RequestInterstitial ();
//		}
//	}


//	void RevMobVideoRequest() {
//		GameObject revMob = GameObject.Find("Revmob");
//		if (revMob != null) {
//			revMob.GetComponent<RevMobSampleAppCSharp> ().RequestVideo ();
//			revMob.GetComponent<RevMobSampleAppCSharp> ().LoadVideoRequest ();
//		}
//	}

}
